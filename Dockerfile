FROM ruby:alpine

# https://github.com/eventmachine/eventmachine/wiki/Building-EventMachine
RUN apk add nodejs npm g++ musl-dev make libstdc++ imagemagick && mkdir /app
WORKDIR /app
RUN gem install bundler
COPY . .
RUN bundle install
expose 3000
CMD ["bundle", "exec", "nanoc", "live"]
