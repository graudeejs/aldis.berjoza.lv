class JpegOptimizationFilter < Nanoc::Filter
  require 'mini_magick'
  identifier :jpeg_optimization
  type :binary

  def run(filename, params={})
    basename = File.basename(filename)
    tmp_file = "tmp_#{basename}"

    image = MiniMagick::Image.open(filename)
    original_size = image.size
    # https://developers.google.com/speed/docs/insights/OptimizeImages
    image.quality params.fetch(:quality, 85)
    image.sampling_factor '4:2:0'
    image.strip
    image.interlace 'JPEG'
    image.colorspace 'sRGB'

    image.write(tmp_file)

    optimized_image =  MiniMagick::Image.open(tmp_file)

    if optimized_image.size < original_size
      FileUtils.mv(tmp_file, output_filename)
    else
      FileUtils.cp(filename, output_filename)
    end
  ensure
    FileUtils.rm_f(tmp_file)
  end
end
