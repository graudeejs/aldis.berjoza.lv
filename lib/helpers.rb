require "mini_magick"
include Nanoc::Helpers::Capturing

module ItemHelpers
  def self.locale(item)
    (item.identifier.to_s.match(/^\/([a-z]{2})\//) || [])[1] || 'en'
  end

  def self.lang_roots(items)
    items.select { |item| item[:lang_root] }.sort_by { |item| ItemHelpers.locale(item) }
  end

  def self.lang_root(items, current_item)
    current_locale = locale(current_item)
    lang_roots(items).find { |item| locale(item) == current_locale }
  end

  def self.menu_items(items, current_item)
    current_locale = locale(current_item)
    items
      .select { |item| item[:show_in_menu] && current_locale == locale_of(item) }
      .sort { |a, b| a.fetch(:menu_priority, 0) <=> b.fetch(:menu_priority, 0) }
  end
end

def render_partial(name)
  @items.find { |i| i.identifier == name }.compiled_content
rescue
  puts "Failed to render '#{name}'\n"
  raise
end

def menu_items
  ItemHelpers.menu_items(items, item)
end

def locale_of(item)
  ItemHelpers.locale(item)
end

def current_locale
  ItemHelpers.locale(item)
end

def lang_roots
  ItemHelpers.lang_roots(items)
end

def lang_root
  ItemHelpers.lang_root(items, item)
end

def import_file(relative_file)
  ERB.new(IO.read(File.expand_path("../#{relative_file}", item.raw_filename))).result(binding)
end

def img_size(image_path)
  MiniMagick::Image.open(image_path).info('dimensions').join('x')
end

def metadata_value(thing, key, localization_scope=nil)
  if thing[:localize_metadata] && !localization_scope.nil?
    t(thing[key], scope: localization_scope)
  else
    thing[key]
  end
end

def menu_title(menu_item)
  metadata_value(menu_item, :menu_title, 'menu_title')
end

def page_title(page)
  metadata_value(page, :page_title, 'page_title') || 'Aldis Berjoza'
end

def gallery_images
  @items.find_all('/images/gallery/*.jpg').sort do |a,b|
    b.fetch(:priority, 10000) <=> a.fetch(:priority, 10000)
  end
end

def relative_identifier(filename)
  components = @item.identifier.components.dup
  components[-1] = filename
  '/' + components.join('/')
end

def find_alt_lang_item(locale)
  return nil if item[:page_id].nil?
  items.find { |i| i[:page_id] == item[:page_id] && locale_of(i) == locale }
end

def find_item(page_id, locale)
  items.find { |i| i[:page_id] == page_id && locale_of(i) == locale }
end
