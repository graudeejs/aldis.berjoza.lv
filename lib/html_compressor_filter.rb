require 'htmlcompressor'

class HtmlCompressorFilter < Nanoc::Filter
  identifier :html_compressor

  def run(content, params={})
    compressor = HtmlCompressor::Compressor.new(compress_js_templates: true, remove_comments: false)
    compressor.compress(content)
  end
end
