class ThumbnailFilter < Nanoc::Filter
  require 'mini_magick'
  identifier :thumbnail
  type :binary

  def run(filename, params={})
    image = MiniMagick::Image.open(filename)
    image.scale params[:size]
    # https://developers.google.com/speed/docs/insights/OptimizeImages
    image.quality params.fetch(:quality, 85)
    image.sampling_factor '4:2:0'
    image.strip
    image.interlace 'JPEG'
    image.colorspace 'sRGB'
    image.write(output_filename)
  end
end
