require 'i18n'

I18n.load_path = Dir[File.expand_path('../i18n/*.yml', __dir__)]
I18n.available_locales = %i[en lv]
I18n.locale = :en

def t(key, user_options={})
  options = {locale: current_locale}.merge(user_options)
  I18n.t(key, **options)
end
